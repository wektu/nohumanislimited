using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.Application;
using Toybox.Time;
using Toybox.Time.Gregorian;
using Toybox.ActivityMonitor;

class NoHumanIsLimitedView extends WatchUi.WatchFace {

    var paranoidOrangeFont = null;
    var paranoidOrangeSmallFont = null;
    var paranoidOrangeMediumFont = null;
	var stepsIcon = null;
	var unreadIcon = null;
	var bgHumanOrange = null; 
        	 

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
    	paranoidOrangeFont = WatchUi.loadResource(Rez.Fonts.ParanoidOrange);
    	paranoidOrangeSmallFont = WatchUi.loadResource(Rez.Fonts.ParanoidOrangeSmall);
    	paranoidOrangeMediumFont = WatchUi.loadResource(Rez.Fonts.ParanoidOrangeMedium);
    	bgHumanOrange = WatchUi.loadResource(Rez.Drawables.NoHumanIsLimitedOrange);
    	
    	var stepsColor = Application.getApp().getProperty("StepsColor");
 	
    	if(stepsColor == 0x000000)
    	{
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsBlack);    	
	    }
	    else if(stepsColor == 0x009DFF)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsBlue);
	    }
	    else if(stepsColor == 0xFF0000)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsRed);
	    }
	    else if(stepsColor == 0xFF5500)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsOrange);
	    }
	    else if(stepsColor == 0xFFFF00)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsYellow);
	    }
	    else if(stepsColor == 0x00FF00)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsGreen);
	    }
	    else if(stepsColor == 0x555555)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsDarkGray);
	    }
	    else if(stepsColor == 0xAAAAAA)
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.StepsLightGray);
	    }
	     else
	    {
	    	stepsIcon = WatchUi.loadResource(Rez.Drawables.Steps);
	    }
	    
	    
	    var unreadColor = Application.getApp().getProperty("UnreadColor");
 	
    	if(unreadColor == 0x000000)
    	{
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadBlack);    	
	    }
	    else if(unreadColor == 0x009DFF)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadBlue);
	    }
	    else if(unreadColor == 0xFF0000)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadRed);
	    }
	    else if(unreadColor == 0xFF5500)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadOrange);
	    }
	    else if(unreadColor == 0xFFFF00)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadYellow);
	    }
	    else if(unreadColor == 0x00FF00)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadGreen);
	    }
	    else if(unreadColor == 0x555555)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadDarkGray);
	    }
	    else if(unreadColor == 0xAAAAAA)
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.UnreadLightGray);
	    }
	     else
	    {
	    	unreadIcon = WatchUi.loadResource(Rez.Drawables.Unread);
	    }
    	
        setLayout(Rez.Layouts.WatchFace(dc));
    }

	

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    
   
    }

    // Update the view
    function onUpdate(dc) {    	
        // Get the current time and format it correctly
        var timeFormat = "$1$$2$";
        var clockTime = System.getClockTime();
        var hours = clockTime.hour;
        if (!System.getDeviceSettings().is24Hour) {
            if (hours > 12) {
                hours = hours - 12;
            }
        }
              
       var today = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
		var dateString = Lang.format(
		    "$1$:$2$:$3$ $4$ $5$ $6$ $7$",
		    [
		        today.hour,
		        today.min,
		        today.sec,
		        today.day_of_week,
		        today.day,
		        today.month,
		        today.year
		    ]
		);
       
       var dayAndDayOfWeek = Lang.format(
		    "$1$, $2$", [today.day_of_week, today.day]
		);
       
       var hoursColor = Application.getApp().getProperty("HoursColor");
       var minutesColor = Application.getApp().getProperty("MinutesColor");
       var backgroundColor = Application.getApp().getProperty("BackgroundColor");
       var stepsColor = Application.getApp().getProperty("StepsColor");
       var dayColor = Application.getApp().getProperty("DayColor");
       var unreadColor = Application.getApp().getProperty("UnreadColor");
       
       var noLimitColor = Application.getApp().getProperty("NoLimitColor");
       var humanIsColor = Application.getApp().getProperty("HumanIsColor");
       
       
       var showDay = Application.getApp().getProperty("ShowDay");
       var showSteps = Application.getApp().getProperty("ShowSteps");
       var showUnread = Application.getApp().getProperty("ShowUnread");
       var hideEmptyUnread = Application.getApp().getProperty("HideEmptyUnread");
       
       var mood = Application.getApp().getProperty("Mood");
              
       var useView = false;
        
        if(useView)
        {
	        var timeString = Lang.format(timeFormat, [hours, clockTime.min.format("%02d")]);
			var hoursString = Lang.format("$1$", [hours]);
			var minutesString = Lang.format("$1$", [clockTime.min.format("%02d")]);
	
	
	        // Update the view
	        var view = View.findDrawableById("HoursLabel");
	        view.setColor(hoursColor);
	        view.setText(hoursString);
	        view.setFont(paranoidOrangeFont);
	        
	        var minutesView = View.findDrawableById("HoursLabel");
	        minutesView.setColor(minutesColor);
	        minutesView.setText(minutesString);
	        minutesView.setFont(paranoidOrangeFont);
	        
	        var dayView = View.findDrawableById("DayLabel");
	        dayView.setColor(dayColor);
	        dayView.setText(dayAndDayOfWeek);
	        dayView.setFont(Graphics.FONT_SYSTEM_XTINY);
	        
	        // get ActivityMonitor info
			var info = ActivityMonitor.getInfo();
			var steps = info.steps;
				
	        var stepsView = View.findDrawableById("StepsLabel");
	        stepsView.setColor(stepsColor);
	        stepsView.setText(Lang.format("$1$", [steps]));
	        stepsView.setFont(Graphics.FONT_SYSTEM_XTINY);
	        
	        // set the text to top of the screen 
	        //view.setLocation(110,42);
	
	        // Call the parent onUpdate function to redraw the layout
	        View.onUpdate(dc);
        }
        else 
        {
        	dc.clear();
        	        	
        	var leftPadding = hours < 10 ? 20 : 0;
        	// draw hours and set color
        	dc.setColor(hoursColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawText(dc.getWidth()/2 - leftPadding, 12, paranoidOrangeFont, hours.toString(), Graphics.TEXT_JUSTIFY_RIGHT);
        	
        	
        	// draw minutes and set color
        	dc.setColor(minutesColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawText(dc.getWidth()/2 - leftPadding, 12, paranoidOrangeFont, Lang.format("$1$", [clockTime.min.format("%02d")]), Graphics.TEXT_JUSTIFY_LEFT);
        	
        
        	
        	// reset the remaining background to black
        	//dc.setColor(backgroundColor, backgroundColor);
        	
        	// draw background
        	//dc.drawBitmap(0, 83, bgHumanOrange);
        	
        	
        	if(mood == 1) // aka optimistic
        	{
        	var xMsg = 30;
        	var yMsg = 100;
        	
        	// draw NO & LIMIT
        	dc.setColor(noLimitColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawText(xMsg, yMsg, paranoidOrangeMediumFont, "NO", Graphics.TEXT_JUSTIFY_LEFT);
        	dc.drawText(xMsg + 25, yMsg + 44, paranoidOrangeMediumFont, "LIMIT", Graphics.TEXT_JUSTIFY_LEFT);
        	
        	// draw HUMAN IS ED
        	dc.setColor(humanIsColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawText(xMsg + 107, yMsg + 44, paranoidOrangeMediumFont, "ED", Graphics.TEXT_JUSTIFY_LEFT);
        	dc.drawText(xMsg + 41, yMsg + 2, paranoidOrangeSmallFont, "HUMAN", Graphics.TEXT_JUSTIFY_LEFT);
        	dc.drawText(xMsg + 109, yMsg + 14, paranoidOrangeSmallFont, "IS", Graphics.TEXT_JUSTIFY_LEFT);
        	}
        	else if(mood == 2)	// aka pessimistic 
        	{
        		var xMsg = 8;
	        	var yMsg = 100;
	        	
	        	// draw NO & LIMIT
	        	dc.setColor(noLimitColor, Graphics.COLOR_TRANSPARENT);
	        	dc.drawText(xMsg, yMsg, paranoidOrangeMediumFont, "SOME", Graphics.TEXT_JUSTIFY_LEFT);
	        	dc.drawText(xMsg + 47, yMsg + 44, paranoidOrangeMediumFont, "LIMIT", Graphics.TEXT_JUSTIFY_LEFT);
	        	
	        	// draw HUMAN IS ED
	        	dc.setColor(humanIsColor, Graphics.COLOR_TRANSPARENT);
	        	dc.drawText(xMsg + 129, yMsg + 44, paranoidOrangeMediumFont, "ED", Graphics.TEXT_JUSTIFY_LEFT);
	        	dc.drawText(xMsg + 84, yMsg + 2, paranoidOrangeSmallFont, "HUMANS", Graphics.TEXT_JUSTIFY_LEFT);
	        	dc.drawText(xMsg + 164, yMsg + 18, paranoidOrangeSmallFont, "ARE", Graphics.TEXT_JUSTIFY_LEFT);
        	}
        	
        		// set color and draw day if needed
        	dc.setColor(dayColor, Graphics.COLOR_TRANSPARENT);
        	if (showDay)
        	{
        		dc.drawText(dc.getWidth()/2, 0, Graphics.FONT_SYSTEM_XTINY, dayAndDayOfWeek.toUpper(), Graphics.TEXT_JUSTIFY_CENTER);
        	}
        	
        	// set color and draw steps if needed
        	dc.setColor(stepsColor, Graphics.COLOR_TRANSPARENT);
        	
        	if(showSteps)
        	{
        		
        		dc.drawBitmap(dc.getWidth()/2 - 30,195, stepsIcon);
        		// get ActivityMonitor info
				var info = ActivityMonitor.getInfo();
				var steps = info.steps;
        		dc.drawText(dc.getWidth()/2 - 10, 193, Graphics.FONT_SYSTEM_XTINY, steps, Graphics.TEXT_JUSTIFY_LEFT);
        	}
        	
        	
        	// set color and draw steps if needed
        	dc.setColor(unreadColor, Graphics.COLOR_TRANSPARENT);
        	var settings= System.getDeviceSettings();
        	var notificationCount = settings.notificationCount;
        	if(notificationCount == null)
        	{
        		notificationCount = 0;
        	}
        	
        	if(hideEmptyUnread && notificationCount == 0)
        	{
        		showUnread = false;
        	}
        		
        	if(showUnread)
        	{
        		var xU = 185;
        		var yU = 75;
        		dc.drawBitmap(xU,yU, unreadIcon);
        		dc.drawText(xU + 8,yU+10, Graphics.FONT_SYSTEM_XTINY, notificationCount, Graphics.TEXT_JUSTIFY_CENTER);
        	}
        	
        	dc.setColor(backgroundColor, backgroundColor);
        }
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }

}
